
var db = firebase.apps[0].firestore();
var container = firebase.apps[0].storage().ref();

const txtPosic = document.querySelector('#txtPosic');
const txtSigno = document.querySelector('#txtSigno');
const txtRango = document.querySelector('#txtRango');
const txtArchi = document.querySelector('#txtArchi');
const element = document.querySelector("#element").value;
const celestialBody = document.querySelector("#celestial-body").value;
const gemstone = document.querySelector("#gemstone").value;
const btnLoad  = document.querySelector('#btnLoad');

btnLoad.addEventListener('click', function(){
    const archivo = txtArchi.files[0];
    const nomarch = archivo.name;
    if(archivo == null){
        alert('Debe seleccionar una imagen');
    }else{
        const metadata = {
            contentType : archivo.type
        }
        const subir = container.child('zodiaco/'+nomarch).put(archivo, metadata);
        subir
            .then(snapshot => snapshot.ref.getDownloadURL())
            .then( url =>{
                db.collection("datosZodiaco").add({
                    "posic" : parseInt(txtPosic.value),
                    "signo" : txtSigno.value,
                    "rango" : txtRango.value,
                    "url"   : url
                }).then(function(docRef) {
                    alert("ID del registro: " + docRef.id);
                    limpiar();
                }).catch(function(FirebaseError) {
                    alert("Error al subir la imagen: " + FirebaseError);
                });
            });
    }
});
function agregarDatos(signo, fecha, elemento, astro, piedra) {
    db.collection("signos").add({
      signo: signo,
      fecha: fecha,
      elemento: elemento,
      astro: astro,
      piedra: piedra
    })
    .then(function(docRef) {
      console.log("Documento escrito con ID: ", docRef.id);
    })
    .catch(function(error) {
      console.error("Error al agregar documento: ", error);
    });
  }
  agregarDatos(signo, fecha, element, celestialBody, gemstone);
function limpiar(){
    txtPosic.value = ''
    txtSigno.value = '';
    txtRango.value = '';
    txtArchi.value = '';
    txtPosic.focus();
}
